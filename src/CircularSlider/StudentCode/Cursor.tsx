import React from "react";
import { StyleSheet } from "react-native";
import { PanGestureHandler, State } from "react-native-gesture-handler";
import Animated, {
  block,
  event,
  set,
  useCode,
  Value,
} from "react-native-reanimated";

import { StyleGuide, withOffset } from "../../components";

import { canvas2Polar, polar2Canvas } from "./Coordinates";

interface ICursorProps {
  theta: Animated.Value<number>;
  r: number;
  size: number;
}

const Cursor = ({ theta, r, size }: ICursorProps) => {
  const state = new Value(State.UNDETERMINED);
  const translationX = new Value(0);
  const translationY = new Value(0);

  const onGestureEvent = event([
    {
      nativeEvent: {
        state,
        translationX,
        translationY,
      },
    },
  ]);
  const center = { x: r, y: r };
  const x = withOffset({ value: translationX, state });
  const y = withOffset({ value: translationY, state });
  const alpha = canvas2Polar({ x, y }, center).theta;
  const { x: translateX, y: translateY } = polar2Canvas(
    { theta: alpha, radius: r },
    center
  );

  useCode(() => block([set(theta, alpha)]), [alpha, theta]);

  return (
    <PanGestureHandler
      onHandlerStateChange={onGestureEvent}
      {...{ onGestureEvent }}
    >
      <Animated.View
        style={{
          ...StyleSheet.absoluteFillObject,
          width: size,
          height: size,
          borderRadius: size / 2,
          borderColor: "white",
          borderWidth: 4,
          backgroundColor: StyleGuide.palette.primary,
          transform: [{ translateX }, { translateY }],
        }}
      />
    </PanGestureHandler>
  );
};

export default Cursor;
