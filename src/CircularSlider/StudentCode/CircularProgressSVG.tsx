import React from "react";
import { StyleSheet } from "react-native";
import Animated, { multiply } from "react-native-reanimated";
import Svg, { Circle } from "react-native-svg";

const AnimatedCircle = Animated.createAnimatedComponent(Circle);

interface ICircularProgressSVGProps {
  theta: Animated.Node<number>;
  r: number;
  bg: string;
  fg: Animated.Node<number>;
  strokeWidth: number;
}

const CircularProgressSVG = ({
  theta,
  r,
  bg,
  fg,
  strokeWidth,
}: ICircularProgressSVGProps) => {
  const radius = r - strokeWidth / 2;
  const strokeDashOffset = multiply(theta, radius);
  const circumference = 2 * Math.PI * radius;

  return (
    <Svg style={StyleSheet.absoluteFill}>
      <Circle
        cx={r}
        cy={r}
        stroke={bg}
        fill="transparent"
        r={radius}
        {...{ strokeWidth }}
      />
      <AnimatedCircle
        cx={r}
        cy={r}
        stroke={fg}
        fill="transparent"
        r={radius}
        strokeDashArray={`${circumference} ${circumference}`}
        {...{ strokeWidth, strokeDashOffset }}
      />
    </Svg>
  );
};

export default CircularProgressSVG;