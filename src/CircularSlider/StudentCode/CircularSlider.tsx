import React from "react";
import { Dimensions, PixelRatio, StyleSheet, View } from "react-native";
import Animated, {
  add,
  cond,
  lessThan,
  multiply,
  sub,
  Value,
} from "react-native-reanimated";

import { StyleGuide } from "../../components";

// import CircularProgress from "./CircularProgress";
import CircularProgressSVG from "./CircularProgressSVG";
import Cursor from "./Cursor";

const { PI } = Math;
const { width } = Dimensions.get("window");
const size = width - 32;
const STROKE_WIDTH = 40;
const r = PixelRatio.roundToNearestPixel(size / 2);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    width: r * 2,
    height: r * 2,
  },
});

const CircularSlider = () => {
  const start = new Value(0);
  const end = new Value(0);
  const theta = sub(
    cond(lessThan(start, end), end, add(multiply(2, PI), end)),
    start
  );
  // const rotate = sub(PI, end);
  const rotate = sub(multiply(2, PI), start);

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Animated.View
          style={{ ...StyleSheet.absoluteFillObject, transform: [{ rotate }] }}
        >
          {/* <CircularProgress
            bg={StyleGuide.palette.background}
            fg={StyleGuide.palette.primary}
            strokeWidth={STROKE_WIDTH}
            {...{ r, theta }}
          /> */}
          <CircularProgressSVG
            bg={StyleGuide.palette.background}
            fg={StyleGuide.palette.primary}
            strokeWidth={STROKE_WIDTH}
            {...{ r, theta }}
          />
        </Animated.View>
        <Cursor theta={start} size={STROKE_WIDTH} r={r - STROKE_WIDTH / 2} />
        <Cursor theta={end} size={STROKE_WIDTH} r={r - STROKE_WIDTH / 2} />
      </View>
    </View>
  );
};

export default CircularSlider;
