import React from "react";
import { PanGestureHandler, State } from "react-native-gesture-handler";
import { StyleSheet } from "react-native";
import Animated, {
  block,
  diffClamp,
  set,
  sub,
  useCode,
} from "react-native-reanimated";

import StyleGuide from "../../components/StyleGuide";
import { withOffset } from "../../components";

const { Value, event } = Animated;

interface IControlPointProps {
  point: {
    x: Animated.Value<number>;
    y: Animated.Value<number>;
  };
  min: number;
  max: number;
}

const ControlPoint = ({ point: { x, y }, min, max }: IControlPointProps) => {
  const translationX = new Value(0);
  const translationY = new Value(0);
  const state = new Value(State.UNDETERMINED)

  const onGestureEvent = event([
    {
      nativeEvent: {
        translationX,
        translationY,
        state,
      },
    },
  ]);

  const x1 = withOffset({ value: translationX, state });
  const y1 = withOffset({ value: translationY, state });

  const translateX = diffClamp(x1, min, max);
  const translateY = diffClamp(y1, min, max);

  useCode(() => block([set(x, translateX), set(y, translateY)]), [
    translateX,
    translateY,
    x,
    y,
  ]);

  return (
    <PanGestureHandler
      onHandlerStateChange={onGestureEvent}
      {...{ onGestureEvent }}
    >
      <Animated.View
        style={{
          ...StyleSheet.absoluteFillObject,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: StyleGuide.palette.primary,
          borderWidth: 4,
          borderColor: "black",
          transform: [
            { translateX: sub(translateX, 20) },
            { translateY: sub(translateY, 20) },
          ],
        }}
      />
    </PanGestureHandler>
  );
};

export default ControlPoint;