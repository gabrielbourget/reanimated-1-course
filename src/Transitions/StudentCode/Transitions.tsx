import React, { useState, useRef } from "react";
import { StyleSheet, ViewStyle, ImageStyle, Dimensions } from "react-native";
import {
  Transitioning,
  Transition,
  TransitioningView,
} from "react-native-reanimated";

import {
  FlexibleCard as Card,
  StyleGuide,
  cards,
  Selection,
} from "../../components";

interface Layout {
  id: string;
  name: string;
  layout: {
    container: ViewStyle;
    child?: ImageStyle;
  };
}

const { width } = Dimensions.get("window");

const transition = (
  <Transition.Change durationMs={400} interpolation="easeInOut" />
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: StyleGuide.palette.background,
  },
});

const layouts: Layout[] = [
  {
    id: "column",
    name: "Column",
    layout: {
      container: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },
  {
    id: "row",
    name: "Row",
    layout: {
      container: {
        flexDirection: "row",
        alignItems: "center",
      },
    },
  },
  {
    id: "wrap",
    name: "Wrap",
    layout: {
      container: {
        flexDirection: "row",
        flexWrap: "wrap",
      },
      child: {
        flex: 0,
        width: width / 2 - StyleGuide.spacing * 2,
      },
    },
  },
];

const Transitions = () => {
  const [currentLayout, setCurrentLayout] = useState(layouts[0].layout);
  const ref = useRef<TransitioningView>(null);

  return (
    <>
      <Transitioning.View
        style={[styles.container, currentLayout.container]}
        {...{ ref, transition }}
      >
        {cards.map((card) => (
          <Card key={card.id} style={currentLayout.child} {...{ card }} />
        ))}
      </Transitioning.View>
      {layouts.map(({ id, name, layout }: Layout) => {
        <Selection
          key={id}
          name={name}
          isSelected={layout === currentLayout}
          onPress={() => {
            if (ref.current) {
              ref.current.animateNextTransition();
            }
            setCurrentLayout(layout);
          }}
        />;
      })}
    </>
  );
};

export default Transitions;
