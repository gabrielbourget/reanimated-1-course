import React from "react";
import { Dimensions, StyleSheet } from "react-native";
import Animated, {
  Value,
  block,
  cond,
  eq,
  set,
  useCode,
  multiply,
} from "react-native-reanimated";
import { PinchGestureHandler, State } from "react-native-gesture-handler";
import {
  onGestureEvent,
  pinchActive,
  pinchBegan,
  timing,
  transformOrigin,
  translate,
  vec,
} from "react-native-redash";

const { width, height } = Dimensions.get("window");
const CANVAS = vec.create(width, height);
const CENTER = vec.divide(CANVAS, 2);
const styles = StyleSheet.create({
  image: {
    ...StyleSheet.absoluteFillObject,
    width: undefined,
    height: undefined,
    resizeMode: "cover",
  },
});

const PinchGesture2 = () => {
  const origin = vec.createValue(0);
  const pinch = vec.createValue(0);
  const focal = vec.createValue(0);
  const gestureScale = new Value(1);
  const numberOfPointers = new Value(0);
  const state = new Value(State.UNDETERMINED);
  const pinchGestureHandler = onGestureEvent({
    numberOfPointers,
    scale: gestureScale,
    state,
    focalX: focal.x,
    focalY: focal.y,
  });
  const scaleOffset = new Value(1);
  const offset = vec.createValue(0, 0);
  const adjustedFocal = vec.sub(focal, vec.add(CENTER, offset));
  useCode(
    () =>
      block([
        cond(pinchBegan(state), vec.set(origin, adjustedFocal)),
        cond(
          pinchActive(state, numberOfPointers),
          vec.set(
            pinch,
            vec.add(
              vec.sub(adjustedFocal, origin),
              origin,
              vec.multiply(-1, gestureScale, origin),
            )
          )
        ),
        cond(eq(state, State.END), [
          set(scaleOffset, multiply(scaleOffset, gestureScale)),
          vec.set(offset, vec.add(offset, pinch)),
          set(gestureScale, 1),
          vec.set(focal, 0),
          vec.set(pinch, 0),
        ]),
      ]),
    [adjustedFocal, numberOfPointers, origin, pinch, gestureScale, state]
  );
  return (
    <PinchGestureHandler {...pinchGestureHandler}>
      <Animated.View style={StyleSheet.absoluteFill}>
        <Animated.Image
          style={[
            styles.image,
            {
              transform: [
                ...translate(vec.add(offset, pinch)),
                { scale: multiply(gestureScale, scaleOffset) },
              ],
            },
          ]}
          source={require("../assets/zurich.jpg")}
        />
      </Animated.View>
    </PinchGestureHandler>
  );
};

export default PinchGesture2;
