import * as React from "react";
import { PanGestureHandler, State } from "react-native-gesture-handler";
import { StyleSheet } from "react-native";
import Animated, {
  and,
  eq,
  clockRunning,
  stopClock,
  cond,
  call,
  neq,
  add,
  not,
  startClock,
  Clock,
  multiply,
  abs,
  sub,
} from "react-native-reanimated";
import { onGestureEvent, min } from "react-native-redash";

const { spring: reSpring } = Animated;

interface PrivateSpringConfig extends Animated.SpringConfig {
  toValue: Animated.Value<number>;
}

export interface WithSpringParams {
  value: Animated.Adaptable<number>;
  velocity: Animated.Adaptable<number>;
  state: Animated.Value<State>;
  snapPoints: Animated.Adaptable<number>[];
  offset?: Animated.Value<number>;
  config?: SpringConfig;
  onSnap?: (value: readonly number[]) => void;
}

type SpringConfig = Omit<Animated.SpringConfig, "toValue">;



const snapPoint = (
  snapPoints: Animated.Adaptable<number>[],
  value: Animated.Adaptable<number>,
  velocity: Animated.Adaptable<number>
) => {
  const point = add(value, multiply(velocity, 0.2));
  const deltas = snapPoints.map((sPoint) => abs(sub(point, sPoint)));
  const minDelta = min(...deltas);

  return snapPoints.reduce((acc, sPoint) =>
    cond(eq(abs(sub(point, sPoint)), minDelta), sPoint, acc)
  );
};

export const withSpring = (props: WithSpringParams) => {
  const {
    value,
    velocity,
    state,
    snapPoints,
    offset,
    config: springConfig,
    onSnap,
  } = {
    offset: new Value(0),
    ...props,
  };
  const clock = new Clock();
  const springState: Animated.SpringState = {
    finished: new Value(0),
    velocity: new Value(0),
    position: new Value(0),
    time: new Value(0),
  };

  const config: PrivateSpringConfig = {
    toValue: new Value(0),
    damping: 20,
    mass: 1,
    stiffness: 150,
    overshootClamping: false,
    restSpeedThreshold: 1,
    restDisplacementThreshold: 1,
    ...springConfig,
  };

  const gestureAndAnimationIsOver = new Value(1);
  const isSpringInterrupted = and(eq(state, State.BEGAN), clockRunning(clock));
  const areGesturesAndAnimationsFinished = new Value(1);

  const finishSpring = [
    set(offset, springState.position),
    stopClock(clock),
    set(gestureAndAnimationIsOver, 1),
  ];
  const snap = onSnap
    ? [cond(clockRunning(clock), call([springState.position], onSnap))]
    : [];
  return block([
    cond(isSpringInterrupted, finishSpring),
    cond(areGesturesAndAnimationsFinished, set(springState.position, offset)),
    cond(gestureAndAnimationIsOver, set(springState.position, offset)),
    cond(neq(state, State.END), [
      set(gestureAndAnimationIsOver, 0),
      set(springState.finished, 0),
      set(springState.position, add(offset, value)),
    ]),
    cond(and(eq(state, State.END), not(gestureAndAnimationIsOver)), [
      cond(and(not(clockRunning(clock)), not(springState.finished)), [
        set(springState.velocity, velocity),
        set(springState.time, 0),
        set(config.toValue, snapPoint(snapPoints, value, velocity)),
        startClock(clock),
      ]),
      reSpring(clock, springState, config),
      cond(springState.finished, [
        set(areGesturesAndAnimationsFinished, 1),
        onSnap && call([springState.position], onSnap),
        ...snap,
        ...finishSpring,
      ]),
    ]),
    springState.position,
  ]);
};

interface SwipeableProps {
  translateX: Animated.Value<number>;
  translateY: Animated.Value<number>;
  snapPoints: number[];
  onSnap: (x: readonly number[]) => void;
  offsetX: Animated.Value<number>;
}

const { Value, useCode, block, set } = Animated;

const Swipeable = ({
  translateX,
  translateY,
  snapPoints,
  onSnap,
  offsetX,
}: SwipeableProps) => {
  const translationX = new Value(0);
  const translationY = new Value(0);
  const velocityX = new Value(0);
  const velocityY = new Value(0);
  const state = new Value(State.UNDETERMINED);
  const gestureHandler = onGestureEvent({
    translationX,
    translationY,
    velocityX,
    velocityY,
    state,
  });

  const x = withSpring({
    value: translationX,
    velocity: velocityX,
    state,
    snapPoints: snapPoints,
    onSnap,
    offset: offsetX,
  });

  const y = withSpring({
    value: translationY,
    velocity: velocityY,
    state,
    snapPoints: [0],
  });

  useCode(() => [block([set(translateX, x), set(translateY, y)])], []);

  return (
    <PanGestureHandler {...{ gestureHandler }}>
      <Animated.View style={StyleSheet.absoluteFill} />
    </PanGestureHandler>
  );
};

export default Swipeable;
