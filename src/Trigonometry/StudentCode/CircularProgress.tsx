import React from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Circle } from "react-native-svg";
import Color from "color";
import Animated from "react-native-reanimated";
import { Feather as Icon } from "@expo/vector-icons";

export const STROKE_WIDTH = 40;
const { PI } = Math;
const { multiply, sub } = Animated;
const AnimatedCircle = Animated.createAnimatedComponent(Circle);
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "center",
  },
  svg: {
    transform: [{ rotateZ: "270deg" }],
  },
});

interface CircularProgressProps {
  icon: string;
  color: string;
  size: number;
  progress: Animated.Node<number>;
}

const CircularProgress = ({
  color,
  size,
  progress,
  icon,
}: CircularProgressProps) => {
  const r = (size - STROKE_WIDTH) / 2;
  const circumference = 2 * Math.PI * r;
  const alpha = multiply(sub(1, progress), Math.PI * 2);
  const strokeDashOffset = multiply(alpha, r);
  const backgroundColor = new Color(color).darken(0.8).string();
  const cx = size / 2;
  const cy = size / 2;

  return (
    <View style={styles.container}>
      <Svg width={size} height={size} style={styles.svg}>
        <Circle
          stroke={backgroundColor}
          fill="none"
          strokeWidth={STROKE_WIDTH}
          {...{ cx, cy, r }}
        />
        <AnimatedCircle
          stroke={color}
          fill="none"
          strokeLinecap="round"
          strokeDashArray={`${circumference}, ${circumference}}`}
          strokeWidth={STROKE_WIDTH}
          {...{
            strokeDashOffset,
            cx,
            cy,
            r,
          }}
        />
      </Svg>
      <View style={styles.container}>
        <Icon
          name={icon}
          size={STROKE_WIDTH}
          color="black"
          style={{ top: -r }}
        />
      </View>
    </View>
  );
};

export default CircularProgress;
