import React from "react";
import { StatusBar } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { enableScreens } from "react-native-screens";

import { LoadAssets, StyleGuide, cards } from "./src/components";
import Examples, { examples } from "./src/Examples";
import ClockValuesAndIdentities from "./src/ClockValuesAndIdentities/StudentCode";
import Transitions from "./src/Transitions";
import UseTransition from "./src/UseTransition/StudentCode";
import Timing from "./src/Timing/StudentCode/Timing";
import DarkMode, { profilePic } from "./src/DarkMode";
import PanGesture from "./src/PanGesture/StudentCode";
import Decay from "./src/Decay/StudentCode";
import Spring from "./src/Spring/StudentCode";
import DynamicSpring from "./src/DynamicSpring/StudentCode";
import DragToSort from "./src/DragToSort/StudentCode";
import Swipe, { profiles } from "./src/Swipe";
import Svg from "./src/Svg/StudentCode";
import Trigonometry from "./src/Trigonometry";
import CircularSlider from "./src/CircularSlider";
import BezierCurves from "./src/BezierCurves";
import PathMorphing from "./src/PathMorphing/StudentCode";
import { pictures } from "./src/PinchGesture";
import PinchGesture from "./src/PinchGesture/StudentCode/PinchGesture3";
import { Lessons } from "./src/components/Routes";

enableScreens();

const fonts = {
  "SFProText-Bold": require("./assets/fonts/SF-Pro-Text-Bold.otf"),
  "SFProText-Semibold": require("./assets/fonts/SF-Pro-Text-Semibold.otf"),
  "SFProText-Regular": require("./assets/fonts/SF-Pro-Text-Regular.otf"),
};

const assets = [
  ...examples.map((example) => example.source),
  ...cards.map((card) => card.source),
  ...profiles.map((profile) => profile.profile),
  profilePic,
  ...pictures,
];

const Stack = createStackNavigator<Lessons>();
const AppNavigator = () => (
  <>
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: StyleGuide.palette.primary,
          borderBottomWidth: 0,
        },
        headerTintColor: "white",
      }}
    >
      <Stack.Screen
        name="Examples"
        component={Examples}
        options={{
          title: "Gestures & Animations",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ClockValuesAndIdentities"
        component={ClockValuesAndIdentities}
        options={{
          title: "Clock Values & Identities",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Transitions"
        component={Transitions}
        options={{
          title: "Transitions",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="useTransition"
        component={UseTransition}
        options={{
          title: "useTransition()",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="DarkMode"
        component={DarkMode}
        options={{
          title: "Dark Mode",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Timing"
        component={Timing}
        options={{
          title: "Timing",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="PanGesture"
        component={PanGesture}
        options={{
          title: "Pan Gesture",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Spring"
        component={Spring}
        options={{
          title: "Spring",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Swipe"
        component={Swipe}
        options={{
          title: "Swipe",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="DynamicSpring"
        component={DynamicSpring}
        options={{
          title: "Dynamic Spring",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="DragToSort"
        component={DragToSort}
        options={{
          title: "Drag to Sort",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Svg"
        component={Svg}
        options={{
          title: "SVG",
          // headerShown: false,
        }}
      />
      <Stack.Screen
        name="Trigonometry"
        component={Trigonometry}
        options={{
          title: "Trigonometry",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Decay"
        component={Decay}
        options={{
          title: "Decay",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="CircularSlider"
        component={CircularSlider}
        options={{
          title: "Circular Slider",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="BezierCurves"
        component={BezierCurves}
        options={{
          title: "Bèzier Curves",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="PathMorphing"
        component={PathMorphing}
        options={{
          title: "Path Morphing",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="PinchGesture"
        component={PinchGesture}
        options={{
          title: "Pinch Gesture",
          // headerShown: false,
        }}
      />
    </Stack.Navigator>
  </>
);

const App = () => (
  <LoadAssets {...{ fonts, assets }}>
    <StatusBar barStyle="light-content" />
    <AppNavigator />
  </LoadAssets>
);

export default App;
